import {createStore, combineReducers, applyMiddleware} from 'redux'
import {counter, counterState} from './reducers/counter';
import {TodosState, todos} from './reducers/todos';
import {UsersState, users} from './reducers/users';


export interface State{
    counter:counterState,
    todos: TodosState,
    users:UsersState
        
    }







const reducer = combineReducers<State>({
    counter:counter,
    todos:todos,
    users
   
})


import thunk from 'redux-thunk'


export const store = createStore<State> (reducer, applyMiddleware(
   thunk))




window['store'] = store
