import {connect} from 'react-redux';
import {selectUser, loadUsers} from '../reducers/users';
import {Dispatch} from 'redux';
import {UsersList} from '../users/usersList';
import {User} from '../users/user';
import {State} from '../store'


const mapStateToProps = (state: State) => {
    return {
        users: state.users.users,
        selectedId: state.users.selected && state.users.selected.id
    }
}

const mapDispatchToProps = (dispatch: Dispatch<State>) => {
    return {
        select(user: User) {
            dispatch(selectUser(user))
        },
        load() {
            dispatch(loadUsers())

        }
    }
}

export const UsersListContainer = connect (
    mapStateToProps,
    mapDispatchToProps
)
(UsersList)