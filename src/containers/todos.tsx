import {connect} from 'react-redux'
import {State} from '../store';
import { Todos } from '../todos/todos';
import {Dispatch} from 'redux';
import {createTodo, archiveTodo} from '../reducers/todos';


const mapStateToProps = (state:State) => ({
    title:'Todos',
    todos: state.todos.current,
    editor:true
})


    const mapDispatchToProps = (dispatch: Dispatch<State>) => ({
    addTodo(title:string){
        dispatch(createTodo(title))
    },
    removeTodo(id:number){
        dispatch(archiveTodo(id))
    }
})

export const CurrentTodos = connect(
    mapStateToProps,
mapDispatchToProps)(Todos)  



