import * as React from 'react'
import {connect} from 'react-redux';
import {Userform} from "../users/userform";
import {State} from "../store";
import {Dispatch} from "redux";


const mapStateToProps = (state: State) => {
    return {
        user: state.users.selected
    }
}

const mapDispatchToProps = (dispatch:Dispatch<State>) => {
    return {
        onSave(){

        },
        onCancel(){

        }
    }
}

export const UserFormContainer = connect( 
    mapStateToProps,
    mapDispatchToProps
)(
    props => (props.user && props.user ?
    <Userform {...props} user={props.user} /> : <p>Pleaase select user</p>)
)