import * as React from 'react'
import { User } from './user'

interface Props {
    user: User
    onSave: (user: User) => void
    onCancel: () => void
}

interface State {
    user: User
}



export class Userform extends React.Component<Props, State>{

    state: State

    constructor(props: Props) {
        super(props);
        this.state = {
            user: props.user
        }
    }



    static getDerivedStateFromProps(nextProps: Props) {
        // componentWillReceiveProps(nextProps:Props){
        //if(nextProps.user ==this.props.user){

        return ({
            user: nextProps.user
        })

    }

    updateField = (event: React.ChangeEvent<HTMLInputElement>) => {
        const elem = event.target,
            fieldName = elem.name,
            fieldValue = elem.value;


        this.setState(prevState => ({
            user: {
                ...prevState.user,
                [fieldName]: fieldValue
            }

        }))
    }

    saveUser = () => {
        this.props.onSave(this.state.user)
    }

    cancel = () => {
        this.props.onCancel && this.props.onCancel()
    }
    render() {
        return <div>


            <div className="form-group">
                <label>Name: </label>
                <input type="text" className="form-control" value={this.state.user.name} onChange={this.updateField} name="name" />

            </div>

            <div className="form-group">
                <label>Email: </label>
                <input type="text" className="form-control" value={this.state.user.email} onChange={this.updateField} name="email" />
                <div>

                    <div className="form-group d-flex justify-content-between">
                        <input type="button" value="cancel" className="btn btn-danger" onClick={this.cancel} />
                        <input type="button" value="saveUser" className="btn btn-success" onClick={this.saveUser} />
                    </div>
                </div></div></div>
    }

}