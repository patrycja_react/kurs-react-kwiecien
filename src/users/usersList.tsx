import * as React from "react";
import { User } from './user';

interface Props {
    users: User[]
    selectedId: User['id'],
    select: (user: User) => any
    load: () => any
}

export const UsersList = (props: Props) =>
    <div><button onClick={props.load}>Load</button>

        <div className="list-group">
            {props.users.map(user =>

                <div className={
                    `list-group-item ${user.id === props.selectedId ? ' active' : ''}`}
                    key={user.id}
                    onClick={() => props.select(user)}>
                    {user.name}

                </div>)}
        </div>
    </div>