import * as React from "react"
import { User } from './user'
import { Userform } from './userform'
import axios from 'axios' //npm i axios --save

interface State {
    users: User[]
    selected: User | null
}

export class Users extends React.Component<{}, State> {

    state: State = {
        users: [{
            id: 123,
            name: "test",
            username: "test",
            email: "test@test"
        }],
        selected: null

    }

    fetchUsers = () => {
        axios.get<User[]>('http://localhost:9000/users')
            .then((response) => {

                this.setState({
                    users: response.data
                })
            })

    }

    select = (selected: User | null) => {
        this.setState({ selected })
    }

    componentDidMount() {
        this.fetchUsers()
    }

    saveUser = (user: User) => {
        axios.put<User>('http://localhost:9000/users/' + user.id, user)
            .then(response => {
                this.select(response.data)
                return this.fetchUsers()
            })
    }

    cancelEdit = () => {
        this.select(null)
    }


    render() {
        const selectedId = this.state.selected && this.state.selected.id || -1
        return <div className="row">
            <div className="col">
                <div className="list-group">
                    {
                        this.state.users.map(user =>

                            <div className={"list-group-item" + (user.id == selectedId ? ' active' : '')}
                                key={user.id} onClick={() => this.select(user)}>
                                {user.name}

                            </div>)

                    }
                </div>


            </div>
            <div className="col">

                {
                    this.state.selected ? <Userform
                        user={this.state.selected}
                        onSave={this.saveUser}
                        onCancel={this.cancelEdit} />
                        :
                        <p>select user</p>
                }


            </div>
        </div>
    }
}