export interface User {
    id: number
    name: string
    username: string
    email:string
    adress?: Address
}

export interface Address {
    street: string 
    suite: string
    city: string
    zipcode: string
}