import { Reducer, ActionCreator } from 'redux'
import { User } from '../users/user'



export interface UsersState {
    users: User[],
    selected: User | null
}

const initialState = {
    users: [],
    selected: null
}

export const users: Reducer<UsersState> = (state = initialState, action: ActionTypes) => {

    switch (action.type) {
        case 'START_LOAD_USERS': return {
            ...state,
            loading: true
        }
        case 'LOAD_USERS': return {
            ...state,
            users: action.payload,
            loading: false
        }

        case 'SELECT_USER': return {
            ...state,
            selected: action.payload
        }

        default:
            return state;
    }
};

type ActionTypes = LOAD_USERS | SELECT_USER | START_LOAD_USERS

interface LOAD_USERS {
    type: 'LOAD_USERS', payload: User[]

}
interface START_LOAD_USERS {
    type: 'START_LOAD_USERS'
}

import axios from 'axios';

export const loadUsers = () => (dispatch: Function) => {

    dispatch({
        type: 'START_LOAD_USERS'
    })

    axios.get<User[]>('http://localhost:9000/users/').then(response => {
        dispatch({
            type: 'LOAD_USERS', payload: response.data
        })
    }, err =>

            dispatch({ type: 'LOAD_USERS_ERROR', payload: err })
    )

}

interface SELECT_USER {
    type: 'SELECT_USER', payload: User
}

export const selectUser: ActionCreator<SELECT_USER> = (user: User) =>
    ({
        type: 'SELECT_USER', payload: user
    })

// export const loadUsers: ActionCreator<LOAD_USERS> = (dispatch) => {
//     dispatch ({
//         type:'LOAD_USERS', payload: []
//     })
// }