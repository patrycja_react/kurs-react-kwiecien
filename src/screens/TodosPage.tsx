import * as React from 'react';
import { Todo } from '../todos/todo';
import { CurrentTodos } from '../containers/todos';



interface State {
    todos: Todo[],
    archived: Todo[]

}




/* addTodo = (title: string) => {
   const todo: Todo = {
     id: Date.now(),
     title: title,
     completed: false
   }
 
 
 
   this.setState(prevState => ({
     //todos:prevState.todos.conacat(todo)
     todos: [...prevState.todos, todo],
   }))
 }*/




export class TodosPage extends React.Component<{}, State>{

    state: State = {
        todos: [
            {
                id: 123,
                title: 'test test',
                completed: false
            }
        ],
        archived: [],
    }

    addTodo = (title: string) => {
        const todo: Todo = {
            id: Date.now(),
            title: title,
            completed: false
        }



        this.setState(prevState => ({
            //todos:prevState.todos.conacat(todo)
            todos: [...prevState.todos, todo],
        }))
    }


    archiveTodo = (id: Todo['id']) => {
        this.setState(prevState => {
            const todo = prevState.todos.find(
                todo => todo.id == id
            )
            return todo ? {
                todos: prevState.todos.filter(
                    todo => todo.id !== id
                ),
                archived: [...prevState.archived, todo]
            } : null

        })
    }

    removeTodo = (id: Todo['id']) => {
        this.setState(prevState => ({
            archived: prevState.archived.filter(
                todo => todo.id !== id
            )
        }))

    }


    render() {

        return <div className="row">
            <div className="col">

                <CurrentTodos />

                {/* <Todos todos={this.state.todos}
                editor={true}
                removeTodo={this.archiveTodo}
                addTodo={this.addTodo} /> */}
            </div>
            <div className="col">

            </div>
        </div>

    }
}