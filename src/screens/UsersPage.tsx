import * as React from 'react';
import { UsersListContainer } from "../containers/usersList";
import { UserFormContainer } from "../containers/userForm";


export class UsersPage extends React.Component {

    render() {
        return <div className="row">

            <div className="col">

                <UsersListContainer />
            </div>

            <div className="col">

<UserFormContainer />
</div>
        </div>
    }
    }