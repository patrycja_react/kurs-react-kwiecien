import * as React from 'react'
import { TodosPage } from './screens/TodosPage';
import { UsersPage } from './screens/UsersPage';

import { Route, Switch, Redirect } from 'react-router-dom'


interface State {
  tab: string
}

class App extends React.Component<{}, State> {

  state: State = {
    tab: 'users'
  }


  setTab = (tab: string) => {
    this.setState({ tab })
  }

  public render() {
    return <React.Fragment>

      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <div className="container">
        <a className="navbar-brand" href="#/">React App</a>

        <div className="collapse navbar-collapse">
          <ul className="navbar-nav">
            <li className="nav-item active">
              <a className="nav-link" href="#/todos">Todos</a>
            </li>
            <li className="nav-item active">
              <a className="nav-link" href="#/users">Users</a>
            </li>
          </ul>
        </div>
        </div>
      </nav>


      <div className="container">
        <div className="row">
          <div className="col">

<Switch>
          <Route path="/" render={(props: any) => <p>Hello</p>} exact={true} />
          <Route path="/todos" component={TodosPage} />
            <Route path="/users" component={UsersPage} />
            <Route path="*" render={(props: any) => <p>Hello!</p>} />

            <Redirect to="/todos" path="*" />
           </Switch>
          </div>
        </div>
      </div>
    </React.Fragment>
  }
}

export default App;
