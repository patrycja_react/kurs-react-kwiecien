import { ChangeEvent } from "react";
import * as React from "react";
import { Todo } from "./todo";
import { ToDoList } from "./ToDoList";

interface State {
    newTitle: string
}


interface Props {
    title: string,
    todos: Todo[],
    editor: boolean,
    addTodo: (title: string) => void
    removeTodo: (id: number) => void
}



export class Todos extends React.Component<Props, State> {

    state: State = {
        newTitle: ''
    }

    static defaultProps = {
        title: 'Todos'
    }

    addTodo() {
        this.props.addTodo(this.state.newTitle)
        this.setState({
            newTitle: ''
        })
    }

    titleChange(event: ChangeEvent<HTMLInputElement>) {
        const value = event.target.value;


        //this.forceUpdate()

        this.setState({
            newTitle: value

        })
    }


    render() {
        return <div>

            <h3>{this.props.title}</h3>
            {this.props.todos.length ?

                <ToDoList todos={this.props.todos} removeTodo={this.props.removeTodo} />
                :
                <p>nothing to show here</p>
                
            }
            {this.props.editor ?

                <div className="input-group mt-2">
                    <input type="text" className="form-control"
                        value={this.state.newTitle}
                        onKeyUp={e => e.key == "Enter" && this.addTodo()}
                        onChange={e => this.titleChange(e)} />

                    <button className="btn" onClick={e => this.addTodo()}>Add</button>

                </div>

                :
                null}
        </div>
    }


componentDidMount(){
    console.log(this.props.title + ' - componentDidMount');
}

componentDidUpdate(){
    console.log(this.props.title + ' - componentDidUpdate');
}

componentReceiveProps(props:Props){
    console.log(this.props.title + ' - componentReceiveProps',
this.props, props);
}

shouldComponentUpdate(nextProps: Props, nextState:State){
    console.log(this.props.title + ' shouldComponentUpdate');
    return (this.state.newTitle != nextState.newTitle)
    || this.props.todos != nextProps.todos;
}


componenttWillMount(){
    console.log(this.props.title + ' - componentWillUnMount');
}


}