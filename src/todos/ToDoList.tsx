import * as React from "react";
import { Todo } from "./todo";

interface Props {
    todos: Todo[]
    removeTodo: (id: Todo['id']) => void
}

export const ToDoList = (props: Props) => {

    return <ul className="list-group">
        {props.todos.map (todo =>
            <li className="list-group-item" key={todo.id}> 
            {todo.title}
            
            
            <span className="close" onClick={
                e => props.removeTodo(todo.id)
            }>&times;</span>
             </li>
        )}
    </ul>

}